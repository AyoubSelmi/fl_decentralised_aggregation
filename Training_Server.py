import flwr as fl
import numpy as np
import os
import tensorflow as tf
from flwr.common.typing import FitRes, Parameters
sessions = []
initial_params = None
# initialise sessions list and initial parameters
class SaveModelStrategy(fl.server.strategy.FedAvg):
    def aggregate_fit(
        self,
        rnd,
        results,
        failures
    ):
        print("getting weights from local storage")
        results_from_IPFS=[]
        for res in results:
            # results: List[Tuple[ClientProxy, FitRes]]
            # FitRes: parameters: Parameters , num_examples: int , metrics: Optional[Metrics] = None
            print ("path=",res[1].metrics["path_weights"])
            print("weights=",res[0])
            if res[1].metrics['path_weights'] is not None:
                try:  
                    client_weights=Parameters(np.load( res[1].metrics['path_weights'] ,allow_pickle=True),"")
                    clientRes=FitRes(client_weights,res[1].num_examples)
                    results_from_IPFS.append((res[0],clientRes))
                    print(" client (",res[0],") has provided the path to its weights" )
                except:
                    print(" client's (",res[0],") weights cannot be loaded")
            #aggregated_weights = super().aggregate_fit(rnd, results_from_IPFS, failures)
        return aggregated_weights

model = tf.keras.applications.EfficientNetB0(
        input_shape=(32, 32, 3), weights=None, classes=10
    )
model.compile("adam", "sparse_categorical_crossentropy", metrics=["accuracy"])

# Create strategy and run server
strategy = SaveModelStrategy(
        fraction_fit=1.0,
        fraction_eval=0.0,
        min_fit_clients=2,
        min_eval_clients=0,
        min_available_clients=2,
        initial_parameters=fl.common.weights_to_parameters(model.get_weights()),
        )
fl.server.start_server(
        server_address = "127.0.0.1:4000",
        config={"num_rounds": 2},
        grpc_max_message_length = 1024*1024*1024,
        strategy = strategy
)
