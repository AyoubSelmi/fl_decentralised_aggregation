import flwr as fl
import os
import numpy as np
from random import randint
import tensorflow as tf
# define date and time to save weights in directories

# Define Flower client
class DecenterClient(fl.client.NumPyClient):
    def __init__(self, model, x_train, y_train, x_test, y_test):
        self.model = model
        self.x_train, self.y_train = x_train, y_train
        self.x_test, self.y_test = x_test, y_test
    def get_parameters(self):
        return self.model.get_weights()

    def fit(self, parameters, config):
        self.model.set_weights(parameters)
        history = self.model.fit(
            self.x_train,
            self.y_train,
            32,
            1,
            validation_split=0.1,
        )

        # Return updated model parameters and results
        parameters_prime = self.model.get_weights()
        
        if not (os.path.exists(f'./Local-weights')):
            os.mkdir(f"./Local-weights")
            
        # Save training weights in the created directory
        filename = f'./Local-weights/{cid}-training-weights.npy'
        np.save(filename, parameters_prime)
        
        num_examples_train = len(self.x_train)
        results = {
            "loss": history.history["loss"][0],
            "accuracy": history.history["accuracy"][0],
            "val_loss": history.history["val_loss"][0],
            "val_accuracy": history.history["val_accuracy"][0],
            "path_weights":filename
        }
        return [], num_examples_train, results
    def evaluate(self, parameters, config):  # type: ignore
           
            return 0.0, 1, {}

def load_partition():
    idx=randint(0, 5)
    """Load 1/10th of the training and test data to simulate a partition."""
    assert idx in range(10)
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar10.load_data()
    return (
        x_train[idx * 5000 : (idx + 1) * 5000],
        y_train[idx * 5000 : (idx + 1) * 5000],
    ), (
        x_test[idx * 1000 : (idx + 1) * 1000],
        y_test[idx * 1000 : (idx + 1) * 1000],
    )

cid=randint(10,10000)
model = tf.keras.applications.EfficientNetB0(
    input_shape=(32, 32, 3), weights=None, classes=10
)
model.compile("adam", "sparse_categorical_crossentropy", metrics=["accuracy"])

(x_train, y_train), (x_test, y_test) = load_partition()

# Start Flower client
client = DecenterClient(model, x_train, y_train, x_test, y_test)
fl.client.start_numpy_client("127.0.0.1:4000", client=client)

